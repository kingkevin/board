# project file name (use for schematic and board layout)
NAME ?= template
SUBSHEET ?= 
# path to KiCad CLI
KICAD = kicad-cli
# path to qeda
QEDA := qeda
# path to KiBOM
KIBOM := kibom
# path to InteractiveHtmlBom
IBOMGEN := ~/.local/share/kicad/8.0/3rdparty/plugins/org_openscopeproject_InteractiveHtmlBom/generate_interactive_bom.py

# read project version
VERSION := $(shell cat version)
# current date for stamping output
DATE = $(shell date +%Y-%m-%d)
# revision based on number of changes on schematic or board layout
REVISION := $(shell git log --pretty=oneline "${NAME}.kicad_sch" "${NAME}.kicad_pcb" | wc -l)

# generate file with version information
VERSIONED_EXT = kicad_sch kicad_pcb kicad_pro json
define version_rule
%.versioned.$1: %.$1
	cp $$< $$@
	sed --in-place 's/\$$$$version\$$$$/${VERSION}/g' $$@
	sed --in-place 's/\$$$$date\$$$$/${DATE}/g' $$@
	sed --in-place 's/\$$$$revision\$$$$/${REVISION}/g' $$@
	sed --in-place 's/\.kicad_sch/.versioned.kicad_sch/g' $$@
endef
$(foreach EXT,$(VERSIONED_EXT),$(eval $(call version_rule,$(EXT))))
VERSIONED_SHEET = $(foreach SHEET,$(NAME) $(SUBSHEET),$(SHEET).versioned.kicad_sch)

FABRICATION_DIR := fabrication
IBOM := ${FABRICATION_DIR}/ibom.html

all: $(VERSIONED_SHEET) ${NAME}.sch.pdf ${NAME}.bom.csv render fab

fab: ${FABRICATION_DIR} ${IBOM}

render: ${NAME}.brd-top.svg ${NAME}.brd-bot.svg ${NAME}.3d.step

# generate fabrication files (gerbers/drill/BoM/PnP)
${FABRICATION_DIR}: ${NAME}.versioned.kicad_sch ${NAME}.versioned.kicad_pcb
	kikit fab jlcpcb --no-drc --assembly --field JLCPCB,LCSC --schematic $^ $@

# generate fabrication files (gerbers/drill/uncorrected PnP)
#${FABRICATION_DIR}: ${NAME}.versioned.kicad_pcb
#	mkdir -p ${FABRICATION_DIR}
#	$(KICAD) pcb export gerbers --output ${FABRICATION_DIR} $<
#	$(KICAD) pcb export drill --output ${FABRICATION_DIR}/ $<
#	$(KICAD) pcb export pos --output ${FABRICATION_DIR}/${NAME}.versioned.pos $<

# generate interactive BoM
${IBOM}: ${NAME}.versioned.kicad_pcb
	python $(IBOMGEN) --no-browser --dest-dir `dirname $@` --name-format `basename $@ ".html"` --show-fields "Value" $< &>/dev/null

# generate symbols and footprints from parts
lib:
	$(QEDA) generate qeda

# generate printable version (PDF) of schematic
%.sch.pdf: %.versioned.kicad_sch %.versioned.kicad_pro
	$(KICAD) sch export pdf --output $@ $<

# generate render from layout (top side)
%.brd-top.svg: %.versioned.kicad_pcb
	$(KICAD) pcb export svg --layers F.Cu,F.Paste,F.Silkscreen,Edge.Cuts --page-size-mode 2 --exclude-drawing-sheet --output $@ $<

# generate render from layout (bottom side)
%.brd-bot.svg: %.versioned.kicad_pcb
	$(KICAD) pcb export svg --layers B.Cu,B.Paste,B.Silkscreen,Edge.Cuts --mirror --page-size-mode 2 --exclude-drawing-sheet --output $@ $<

# export Bill of Material (as CSV)
%.bom.xml: %.versioned.kicad_sch %.versioned.kicad_pro
	$(KICAD) sch export python-bom --output $@ $<

# export 3D model
%.3d.step: %.versioned.kicad_pcb
	$(KICAD) pcb export step --output $@ $<

# export Bill of Material (as CSV)
%.bom.csv: %.bom.xml
	$(KIBOM) $< $@

# generate panel
PANEL_DIR := panel_fab

panel: panel.kicad_pcb panel.brd-top.svg panel.brd-bot.svg panel.brd-top.png panel.brd-bot.png ${PANEL_DIR}

panel.kicad_pcb: ${NAME}.versioned.kicad_pcb ${NAME}.versioned.kicad_pro ${NAME}.versioned.kicad_sch panel.versioned.json
	kikit panelize -p panel.versioned.json ${NAME}.versioned.kicad_pcb $@
	sed --in-place 's/\"missing_courtyard\": \"warning\"/\"missing_courtyard\": \"ignore\"/g' $(patsubst %.kicad_pcb,%.kicad_pro,$@) # the mouse bites don't have a courtyard

${PANEL_DIR}: ${NAME}.versioned.kicad_sch panel.kicad_pcb
	kikit fab jlcpcb --assembly --missingError --field JLCPCB,LCSC --schematic $^ $@

clean:
	rm -f $(foreach EXT,$(VERSIONED_EXT),${NAME}.versioned.$(EXT))
	rm -f ${NAME}.sch.pdf ${NAME}.brd-top.png ${NAME}.brd-bot.png ${NAME}.brd-top.svg ${NAME}.brd-bot.svg ${NAME}.versioned.xml ${NAME}.bom.csv
	rm -f ${NAME}.versioned.kicad_prl ${NAME}.versioned.kicad_pro-bak ${NAME}.versioned.xml ${NAME}.versioned.csv
	rm -f ${IBOM}
	rm -rf ${FABRICATION_DIR}
	rm -f panel.versioned.json panel.kicad_pcb panel.kicad_pro panel.brd-top.svg panel.brd-bot.svg panel.brd-top.png panel.brd-bot.png
	rm -rf ${PANEL_DIR}
